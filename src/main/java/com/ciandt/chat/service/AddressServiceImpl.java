package com.ciandt.chat.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ciandt.chat.model.Address;
import com.ciandt.chat.repository.AddressRepository;

@Service
public class AddressServiceImpl implements  AddressService{

	@Autowired
	private AddressRepository addressRepository;

	@Override
	public List<Address> findAllAddress() {
		return (List<Address>) addressRepository.findAll();
	}

	@Override
	public List<Address> findByUser(Integer userID) {
		return (List<Address>) addressRepository.findOne(userID.longValue());
	}

}
