package com.ciandt.chat.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.ciandt.chat.model.Message;
import com.ciandt.chat.model.TypeMessage;
import com.ciandt.chat.model.User;
import com.ciandt.chat.repository.MessageRepository;

public class MessageService implements MessageServiceImpl {

	@Autowired
	private MessageRepository messageRepository;

	@Transactional
	public Message saveMessage(Message message) {
		return messageRepository.save(message);
	}

	@Transactional
	@Override
	public List<Message> findMessageByTokenAndUser(String token, User user) {
		return messageRepository.findByUserAndToken(user, token);
	}

	@Transactional
	@Override
	public Message findManualResponseMessage(User user, Message messageRequest,
			TypeMessage typeMessage) {
		return messageRepository.findByUserAndRequestAndTypeMessage(user,
				messageRequest, typeMessage);
	}

	@Transactional
	@Override
	public List<Message> findLastMessagesByUser(Integer idUser) {
		List<Message> messages = messageRepository
				.findLastTokenUsedByUser(idUser);
		List<Message> onlyLastMessages = new ArrayList<Message>();
		String token = null;

		if (messages.size() > 0) {
			token = messages.get(0).getToken();
		}

		for (Message message : messages) {
			if (message.getToken().equals(token)) {
				onlyLastMessages.add(message);
			} else {
				break;
			}
		}

		return onlyLastMessages;

	}

	@Override
	public Message saveMessage(String newMessage, String token, User user,
			TypeMessage typeMessage) {
		Message message = saveMessage(token, newMessage, "KATRINA OPERADORA",
				user, typeMessage, null);
		return message;
	}

	@Override
	public Message saveMessage(String newMessage, String token, User user,
			TypeMessage typeMessage, Message requestMessage) {
		Message message = saveMessage(token, newMessage, "KATRINA OPERADORA",
				user, typeMessage, requestMessage);
		return message;
	}

	@Override
	public List<Message> findAndAddOldMessages(String token, User user) {
		return findMessages(token, user);
	}

	private List<Message> findMessages(String token, User user) {
		return findMessageByTokenAndUser(token, user);
	}

	private Message saveMessage(String token, String newMessage,
			String operatorName, User user, TypeMessage typeMessage,
			Message requestMessage) {
		Message message = new Message(new Date().getTime(), token, newMessage,
				operatorName, user, typeMessage, requestMessage);
		return saveMessage(message);
	}

}
