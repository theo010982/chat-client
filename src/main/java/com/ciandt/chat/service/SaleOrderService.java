package com.ciandt.chat.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ciandt.chat.model.SaleOrder;
import com.ciandt.chat.model.User;
import com.ciandt.chat.repository.SaleOrderRepository;

@Transactional
@Service
public class SaleOrderService {

    @Autowired
    private SaleOrderRepository orderRepository;

    public List<SaleOrder> findOrderByUser(User user){
    	return orderRepository.findByUser(user);
    }

	public SaleOrder findOrderByNumber(Integer orderNumber) {
		return orderRepository.findOne(orderNumber.longValue());
	}
    
}
