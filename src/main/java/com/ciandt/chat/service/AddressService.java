package com.ciandt.chat.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.ciandt.chat.model.Address;

@Service
public interface AddressService {

	List<Address> findAllAddress();
	
	List<Address> findByUser(Integer id);

}
