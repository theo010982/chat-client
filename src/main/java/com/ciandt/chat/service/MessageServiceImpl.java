package com.ciandt.chat.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.ciandt.chat.model.Message;
import com.ciandt.chat.model.TypeMessage;
import com.ciandt.chat.model.User;

@Service
public interface MessageServiceImpl {

	List<Message> findMessageByTokenAndUser(String token, User user);

	Message findManualResponseMessage(User user, Message messageRequest,
			TypeMessage typeMessage);

	List<Message> findLastMessagesByUser(Integer idUser);

	Message saveMessage(String newMessage, String token, User user,
			TypeMessage typeMessage);

	Message saveMessage(String newMessage, String token, User user,
			TypeMessage typeMessage, Message requestMessage);

	List<Message> findAndAddOldMessages(String token, User user);

}
