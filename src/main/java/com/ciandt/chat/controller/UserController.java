package com.ciandt.chat.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ciandt.chat.model.User;
import com.ciandt.chat.service.UserService;

@Controller
@RequestMapping(value = "/user")
public class UserController {

	@Autowired
	private UserService userService;

	@RequestMapping(value = "/findUserByEmail/{email}/", method = RequestMethod.GET)
	public @ResponseBody User findUserByEmail(@PathVariable(value = "email") String email) {
		return userService.findByEmail(email);
	}
	
	@RequestMapping(value = "/findUserByID/{id}/", method = RequestMethod.GET)
	public @ResponseBody User findUserByName(@PathVariable(value = "id") Integer id) {
		return userService.findById(id);
	}
	
	@RequestMapping(value = "/findAll", method = RequestMethod.GET)
	public @ResponseBody List<User> findAll() {
		return userService.findAll();
	}
}
