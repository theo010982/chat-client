package com.ciandt.chat.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ciandt.chat.model.SaleOrder;
import com.ciandt.chat.model.User;
import com.ciandt.chat.service.SaleOrderService;
import com.ciandt.chat.service.UserService;

@Controller
@RequestMapping(value = "/saleOrder")
public class SaleOrderController {

	@Autowired
	private SaleOrderService saleOrderService;

	@Autowired
	private UserService userService;

	@RequestMapping(value = "findOrderByNumber/{orderNumber}", method = RequestMethod.GET)
	public @ResponseBody SaleOrder findOrderByNumber(
			@PathVariable(value = "orderNumber") Integer orderNumber) {
		return saleOrderService.findOrderByNumber(orderNumber);
	}

	@RequestMapping(value = "findOrderByUser/{idUser}", method = RequestMethod.GET)
	public @ResponseBody List<SaleOrder> findOrderByUser(@PathVariable(value = "idUser") Integer idUser) {
		User user = userService.findById(idUser);
		return saleOrderService.findOrderByUser(user);
	}
}
