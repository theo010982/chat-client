package com.ciandt.chat.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ciandt.chat.model.Message;
import com.ciandt.chat.model.TypeMessage;
import com.ciandt.chat.model.User;
import com.ciandt.chat.repository.MessageRepository;
import com.ciandt.chat.repository.UserRepository;

@Controller
@RequestMapping(value = "/message")
public class MessageController {

    @Autowired
    private MessageRepository messageRepository;
    
    @Autowired
    private UserRepository userRepository;
    
    @RequestMapping(value = "findMessageByUserID/{id}/", method = RequestMethod.GET)
    public @ResponseBody List<Message> findMessageByUserID(@PathVariable(value = "id") Integer id) {
    	return (List<Message>) messageRepository.findLastTokenUsedByUser(id);
    }
    
    
    @RequestMapping(value = "/findByUser", method = RequestMethod.GET)
    public @ResponseBody List<Message> findByUser() {
    	return (List<Message>) messageRepository.findAll();
    }

    @RequestMapping(value = "/findByUserAndToken/{userID}/{token}", method = RequestMethod.GET)
    public @ResponseBody List<Message> findByUserAndToken(@PathVariable(value = "userID") Integer userID,
    		@PathVariable(value = "token") String token) {
    	User user =	userRepository.findOne(userID);
    	return messageRepository.findByUserAndToken(user, token);
    }
    
    
    @RequestMapping(value = "findByUserAndRequestAndTypeMessage/{userID}/{requestMessageID}/{typeMessage}",
    		method = RequestMethod.GET)
    public @ResponseBody Message findByUserAndRequestAndTypeMessage(
    		@PathVariable(value = "userID") Integer userID,
    		@PathVariable(value = "requestMessageID") Integer requestMessageID,
    		@PathVariable(value = "typeMessage") TypeMessage typeMessage) {
    	
    	User user =	userRepository.findOne(userID);
    	Message messageRequest = messageRepository.findOne(requestMessageID.longValue());
    	
    	return messageRepository.findByUserAndRequestAndTypeMessage(user, messageRequest, typeMessage);
    }
    
    @RequestMapping(value = "/findMessageByLastMessageID/{lastMessageId}", method = RequestMethod.GET)
    public @ResponseBody Message findByUserAndToken(@PathVariable(value = "lastMessageId") Integer lastMessageId) {
    	return messageRepository.findOne(lastMessageId.longValue());
    }
    
    @RequestMapping(value = "/saveMessage", method = RequestMethod.POST)
    public @ResponseBody Message saveMessage(@RequestBody Message message) {
    	return messageRepository.save(message);
    }
}
