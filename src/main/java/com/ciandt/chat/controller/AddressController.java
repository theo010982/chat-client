package com.ciandt.chat.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ciandt.chat.model.Address;
import com.ciandt.chat.service.AddressService;

@Controller
@RequestMapping(value = "/address")
public class AddressController {

	@Autowired
	private AddressService addressService;

	@RequestMapping(value = "/findAllAddress", method = RequestMethod.GET)
	public @ResponseBody
	List<Address> findAllAddress() {
		return addressService.findAllAddress();
	}

	@RequestMapping(value = "findMessageByUserID/{id}/", method = RequestMethod.GET)
	public @ResponseBody
	List<Address> findMessageByUserID(@PathVariable(value = "id") Integer id) {
		return (List<Address>) addressService.findByUser(id);
	}

}
