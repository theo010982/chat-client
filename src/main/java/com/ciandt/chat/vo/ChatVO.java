package com.ciandt.chat.vo;

import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ChatVO {

    private List<ChatMessagesVO> message;
    private String token;

    /**
     * @return the message
     */
    public List<ChatMessagesVO> getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(List<ChatMessagesVO> message) {
        this.message = message;
    }

    /**
     * @return the token
     */
    public String getToken() {
	return token;
    }

    /**
     * @param token the token to set
     */
    public void setToken(String token) {
	this.token = token;
    }
     
}
