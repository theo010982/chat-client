package com.ciandt.chat.helper;
import java.util.ArrayList;
import java.util.List;

import com.ciandt.chat.model.Message;
import com.ciandt.chat.vo.ChatMessagesVO;
import com.ciandt.chat.vo.ChatVO;

public class MessageHelper {

    public static ChatMessagesVO createVOMessage(Message message) {
	ChatMessagesVO chatMessagesVO = new ChatMessagesVO(message);
	return chatMessagesVO;
    }

    public static ChatVO createVOMessage(List<Message> messages) {
	ChatVO chatVO = new ChatVO();
	List<ChatMessagesVO> messagesVO = new ArrayList<ChatMessagesVO>();
	for (Message message : messages) {
	    messagesVO.add(createVOMessage(message));
	}
	chatVO.setMessage(messagesVO);
	chatVO.setToken(messagesVO.get(0).getToken());
	return chatVO;
    }

}
