package com.ciandt.chat.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.ciandt.chat.model.SaleOrder;
import com.ciandt.chat.model.User;

public interface SaleOrderRepository extends CrudRepository<SaleOrder, Long>{

    List<SaleOrder> findByUser(User user);
    
}
