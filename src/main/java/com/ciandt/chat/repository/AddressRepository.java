package com.ciandt.chat.repository;

import org.springframework.data.repository.CrudRepository;

import com.ciandt.chat.model.Address;

public interface AddressRepository extends CrudRepository<Address, Long>{
    
}
