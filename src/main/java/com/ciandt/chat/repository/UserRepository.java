package com.ciandt.chat.repository;

import org.springframework.data.repository.CrudRepository;

import com.ciandt.chat.model.User;

public interface UserRepository extends CrudRepository<User, Integer> {
	
    User findByEmail(String email);
    
    User findByName(String name);

}
