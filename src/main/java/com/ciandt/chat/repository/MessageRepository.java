package com.ciandt.chat.repository;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.ciandt.chat.model.Message;
import com.ciandt.chat.model.TypeMessage;
import com.ciandt.chat.model.User;

public interface MessageRepository extends CrudRepository<Message, Long> {

    List<Message> findByUserAndToken(User user, String token);
    
    List<Message> findByToken(String token);
    
    List<Message> findByUser(User user);
    
    Message findByUserAndRequestAndTypeMessage(User user, Message request, TypeMessage typeMessage);
    
    Message findByUserAndTypeMessage(User user, Message request, TypeMessage typeMessage);

    @Query("SELECT m from Message m where m.user.id = :idUser order by m.data desc")
    List<Message> findLastTokenUsedByUser(@Param("idUser") Integer idUser);
    
    
}
