package com.ciandt.chat.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;


@Entity
public class SaleOrder {
   
    @Id
    @GeneratedValue
    private int id;

    private Long number;
    
    private Long totalValue;
    
    @OneToOne
    @JoinColumn(name = "id_user")
    @ForeignKey(name="Fk_order_user")
    private User user;
    
    @OneToOne
    @JoinColumn(name = "id_address")
    @ForeignKey(name="Fk_order_address")
    private Address deliveryAddress;
    
    @Enumerated(EnumType.STRING)
    @Column(name="StatusOrder" ) 
    private StatusOrder statusOrder;
    
    @OneToMany
    @JoinColumn(name = "id_sale_order")
    @ForeignKey(name="Fk_order_product")
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<SaleProduct> products;
    
   
    /**
     * @return the number
     */
    public Long getNumber() {
        return number;
    }

    /**
     * @param number the number to set
     */
    public void setNumber(Long number) {
        this.number = number;
    }

     /**
     * @return the totalValue
     */
    public Long getTotalValue() {
        return totalValue;
    }

    /**
     * @param totalValue the totalValue to set
     */
    public void setTotalValue(Long totalValue) {
        this.totalValue = totalValue;
    }

    /**
     * @return the user
     */
    public User getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * @return the deliveryAddress
     */
    public Address getDeliveryAddress() {
        return deliveryAddress;
    }

    /**
     * @param deliveryAddress the deliveryAddress to set
     */
    public void setDeliveryAddress(Address deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    /**
     * @return the order
     */
    public StatusOrder getStatusOrder() {
        return statusOrder;
    }

    /**
     * @param order the order to set
     */
    public void setStatusOrder(StatusOrder order) {
        this.statusOrder = order;
    }
    
    
    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the products
     */
    public List<SaleProduct> getProducts() {
        return products;
    }

    /**
     * @param products the products to set
     */
    public void setProducts(List<SaleProduct> products) {
        this.products = products;
    }
    
    
}
