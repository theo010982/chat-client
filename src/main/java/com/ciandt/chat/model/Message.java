package com.ciandt.chat.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import org.hibernate.annotations.ForeignKey;

@Entity
public class Message {

    @Id
    @GeneratedValue
    private Long id;

    private Long orderMessage;

    private String token;

    private Date data;
    
    /**
     * Tipo da mensagem, pode ser do usuario, do Robo ou de uma pessoa respondendo o chat.
     * 
     */
    @Enumerated(EnumType.STRING)
    @Column(name="typeMessage" ) 
    private TypeMessage typeMessage;
    
    /**
     * Mantem a referencia para a pergunta que gerou a esta resposta
     */
    @OneToOne
    @ForeignKey(name="Fk_message_message")
    private Message request;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_usuario")
    @ForeignKey(name="Fk_message_user")
    private User user;

    private String message;

    private String operatorName;

    public Message() {
	super();
    }

    public Message(Long orderMessage, String token, String message, String operatorName,
	    User user, TypeMessage typeMessage, Message requestMessage) {
	super();
	this.orderMessage = orderMessage;
	this.token = token;
	this.message = message;
	this.operatorName = operatorName;
	this.data = new Date();
	this.user = user;
	this.typeMessage = typeMessage;
	this.request = requestMessage;
    }

   

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the orderMessage
     */
    public Long getOrderMessage() {
	return orderMessage;
    }

    /**
     * @param orderMessage
     *            the orderMessage to set
     */
    public void setOrderMessage(Long orderMessage) {
	this.orderMessage = orderMessage;
    }

    /**
     * @return the token
     */
    public String getToken() {
	return token;
    }

    /**
     * @param token
     *            the token to set
     */
    public void setToken(String token) {
	this.token = token;
    }

    /**
     * @return the data
     */
    public Date getData() {
	return data;
    }

    /**
     * @param data
     *            the data to set
     */
    public void setData(Date data) {
	this.data = data;
    }

    /**
     * @return the user
     */
    public User getUser() {
	return user;
    }

    /**
     * @param user
     *            the user to set
     */
    public void setUser(User user) {
	this.user = user;
    }

    /**
     * @return the message
     */
    public String getMessage() {
	return message;
    }

    /**
     * @param message
     *            the message to set
     */
    public void setMessage(String message) {
	this.message = message;
    }

    /**
     * @return the operatorName
     */
    public String getOperatorName() {
	return operatorName;
    }

    /**
     * @param operatorName
     *            the operatorName to set
     */
    public void setOperatorName(String operatorName) {
	this.operatorName = operatorName;
    }

    /**
     * @return the typeMessage
     */
    public TypeMessage getTypeMessage() {
        return typeMessage;
    }

    /**
     * @param typeMessage the typeMessage to set
     */
    public void setTypeMessage(TypeMessage typeMessage) {
        this.typeMessage = typeMessage;
    }

    /**
     * @return the request
     */
    public Message getRequest() {
        return request;
    }

    /**
     * @param request the request to set
     */
    public void setRequest(Message request) {
        this.request = request;
    }
   

    
}
