package TestSpringRestExample;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.web.client.RestTemplate;

import com.ciandt.chat.model.Address;


public class AddressTest {
    
    @SuppressWarnings("unchecked")
    @Test
    public void test() {
	RestTemplate restTemplate = new RestTemplate();
	List<Address> response = restTemplate.getForObject("http://localhost:9999/chat/address/findAllAddress", List.class);
	Assert.assertEquals("13251-180", response.get(0).getPostalCode());
    }

}
