package com.ciandt.chat.seleniumCore;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public abstract class SeleliumBase {

	private static WebDriver driver;

	public SeleliumBase() {
		super();
	}

	protected void openFireFoxDriver() {
		driver = new FirefoxDriver();
	}

	protected void openUrl(String url) {
		driver.get(url);
	}

	protected void fillElementById(String elementName, String value) {
		findElement(By.id(elementName)).sendKeys(value);
	}

	protected void fillElementByClassName(String elementName, String value) {
		findElement(By.className(elementName)).sendKeys(value);
	}

	protected void clickElementById(String elementName) {
		findElement(By.id(elementName)).click();
	}
	
	protected String getTextElementByClassName(String idElement) {
		return findElement(By.className(idElement)).getText();
	}

	private WebElement findElement(By by) {
		return driver.findElement(by);
	}
	
	public void closeBrowser() {
		driver.close();
	}

}
