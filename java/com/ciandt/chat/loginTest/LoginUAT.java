package com.ciandt.chat.loginTest;


import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import com.ciandt.chat.home.HomePage;
import com.ciandt.chat.login.LoginPage;

public class LoginUAT {
	
	@Test
	public void myFirtLoginInSelenium() {
		 
		 WebDriver driver  = new FirefoxDriver();
		 driver.manage().window().maximize();
		 driver.get("localhost:9999/");  
		 WebElement findElement = driver.findElement(By.id("j_username"));
		 findElement.sendKeys("theo@gmail.com");
		 driver.findElement(By.id("j_password")).sendKeys("123");
		 driver.findElement(By.id("btnLogin")).click();
		 String welcome = driver.findElement(By.className("muted")).getText();
		 Assert.assertEquals("Bem Vindo a carteira de vacinação.", welcome);
		 
		 driver.close();
	}
	
	@Test
	public void mySecondLoginInSelenium() {
		 LoginPage login = new LoginPage();
		 login.openLoginURL();
		 login.loggerUser();
		 
		 HomePage hPage = new HomePage();
		 hPage.verifyIsLogged();
		 hPage.getContactsPage();
		 hPage.closeBrowser();
		 
		 
	}
	
}
